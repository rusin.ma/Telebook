//
//  Date.hpp
//  Telebook
//
//  Created by Marta Rusin on 05/11/2022.
//


#pragma once


class Date {
    int day, month, year;
   
    
public:
    Date();
    Date(int day, int month, int year);
    
    void setDay(int);
    void setMonth(int);
    void setYear(int);
    
    int getDay();
    int getMonth();
    int getYear();
    
    void display();
};
