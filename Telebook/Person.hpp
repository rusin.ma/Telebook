//
//  Person.hpp
//  Telebook
//
//  Created by Marta Rusin on 05/11/2022.


#include<string>
using namespace std;
#pragma once
#include"Date.hpp"


class Person {

    string name;
    string surname;
    Date dateOfBirth;

public:
    Person();
    Person(string name, string surName, Date dateOfBirth);

    void setName(string);
    void setSurname(string);
    void setDateOfBirth(Date);

    string getName();
    string getSurname();
    Date getDateOfBirth();
    
    void display();

};
