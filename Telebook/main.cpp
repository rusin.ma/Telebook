//
//  main.cpp
//  Telebook
//
//  Created by Marta Rusin on 05/11/2022.
//

#include <iostream>
#include "Date.hpp"
#include "Person.hpp"
using namespace std;

int main() {
    
    Date myDate = Date();
    
    cout<< "Wyswietl date: "<< endl;
    myDate.display();
    
    Person myPerson = Person();
    cout<< "Wyswietl Person: " << endl;
    myPerson.display();
    
    cout << "\n --------------------------------- \n ";
    Date realDate = Date(2, 12, 1978);
    Person definedPerson = Person("Marta", "Rusin", realDate);
    cout<< "Wyświetl Person z danymi: " <<endl;
    definedPerson.display();
    
    cout<< "Miesiac z obiektu definedPerson: " << endl;
    cout <<definedPerson.getDateOfBirth().getMonth() <<endl;
    
    cout << "\n --------------------------------- \n ";
    Person anotherPerson = Person("Adam", "Mickiewicz", Date(5,12,1981));
    cout << "Wyswietl anotherPersion: \n";
    anotherPerson.display();
    
    anotherPerson.setDateOfBirth(Date(anotherPerson.getDateOfBirth().getDay(),anotherPerson.getDateOfBirth().getMonth(),1980));
    cout << "Wyswietl anotherPersion: \n";
    anotherPerson.display();
    
    return 0;
}
