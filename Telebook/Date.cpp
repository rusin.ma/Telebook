//
//  Date.cpp
//  Telebook
//
//  Created by Marta Rusin on 05/11/2022.
//

#include "Date.hpp"
#include<iostream>

using namespace std;

Date::Date(): day(1), month(0), year(1900)
{
    cout <<"Dziala konstruktor klasy Date bez arg." <<endl;
    
}

Date::Date(int day, int month, int year):
day(day), month(month), year(year)

{
    cout<< "Działa konstruktor klasy Date z arg. " << endl;
    while(month<=0 || month>12) {
        cout << "Podales zle dane: " << endl;
        cin >> month;

    }
}

void Date::setDay(int day) {
    this -> day = day; //"this ->" referencja
}

void Date::setMonth(int month) {
    while(month<=0 || month>12) {
        cout << "Podales zle dane: " << endl;
        cin >> month;

    }
    this -> month = month;
   
    
}

void Date::setYear(int year) {
    this -> year = year;
}

int Date::getDay() {
    return this -> day; //moze byc z "this" albo bez "this"
}

int Date::getMonth() {
    return month;
}

int Date::getYear() {
    return year;
}

void Date::display()
{
    cout << "Dzien-Miesiac-Rok: " << day << "-" << month <<"-" << year << endl;
}
