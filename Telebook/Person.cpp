//
//  Person.cpp
//  Telebook
//
//  Created by Marta Rusin on 05/11/2022.
//

#include "Person.hpp"
#pragma once
#include <iostream>
using namespace std;

Person::Person():name("John"), surname("Doe")
{
    cout << "Dziala konstruktor klasy Person, bez arg. " << endl;
}


Person::Person(string name, string surname, Date dateOfBirth):
name(name), surname(surname), dateOfBirth(dateOfBirth)
{
    cout << "Dziala konstruktor klasy Person z argumentami " <<endl;
}

void Person::setName(string name) {
    this-> name = name;
}

void Person::setSurname(string surname) {
    this -> surname = surname;
}

void Person::setDateOfBirth(Date date) {
    this -> dateOfBirth = date;
}

string Person::getName()
{
    return this->name;
}

string Person::getSurname()
{
    return this->surname;
}

Date Person::getDateOfBirth() {
    return this->dateOfBirth;
}

void Person::display() {
    cout << "Imie i Nazwisko: " << this->name << " " << this -> surname << endl;
    this->dateOfBirth.display();
}
